public class User {

    private int    emploeeId;
    private String name;
    private String lastName;
    protected int  salary;


    public int getterEmploeeId() {
        return emploeeId;
    }


    public void setterEmploeeId(int emploeeId) {
        this.emploeeId = emploeeId;
    }


    public String getterName() {
        return name;
    }


    public void setterName(String name) {
        this.name = name;
    }


    public String getterLastName() {
        return lastName;
    }


    public void setterLastName(String lastName) {
        this.lastName = lastName;
    }


    public int getterSalary() {
        return salary;
    }


    public void setterSalary(int salary) {
        this.salary = salary;
    }

}
